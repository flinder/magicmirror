stages:
  - build
  - test

include:
  - template: Security/Container-Scanning.gitlab-ci.yml
  - project: "khassel/container"
    file: "/.yaml/docker_readme.yaml"

image: ${CI_REGISTRY}/khassel/container/kaniko:latest

variables:
  GIT_DEPTH: 1
  MAGICMIRROR_VERSION: "v2.20.0"
  NODE_VERSION_MASTER: "16"
  NODE_VERSION_DEVELOP: "18"
  DEBIAN_VERSION: "bullseye"
  DEBIAN_VERSION_ARM: "buster"
  GitRepo: "https://github.com/MichMich/MagicMirror.git"
  TOOLBOX_IMG: "${CI_REGISTRY}/khassel/container/toolbox:latest"

.rule:
  rules:
  - if: $TASK == "runtime"

.beforescript:
  before_script:
    - docker.gitlab.login
    - |
      set -e
      # set build arch
      if [ "${imgarch}" = "arm" ]; then
        export buildarch="arm32v7/"
        export DEBIAN_VERSION=${DEBIAN_VERSION_ARM}
      elif [ "${imgarch}" = "arm64" ]; then
        export buildarch="arm64v8/"
      elif [ "${imgarch}" = "debug" ]; then
        export debug="true"
      elif [ ! "${imgarch}" = "amd64" ]; then
        echo "unsupported image arch: ${imgarch}"
        exit 1
      fi
      # master or not
      if [ "${CI_COMMIT_BRANCH}" = "master" ]; then
        BuilderTag=${MAGICMIRROR_VERSION}
        export NODE_VERSION=${NODE_VERSION_MASTER}
      else
        BuilderTag=${CI_COMMIT_BRANCH}
        export NODE_VERSION=${NODE_VERSION_DEVELOP}
      fi
      export BUILDER_IMG="${CI_REGISTRY_IMAGE}:${BuilderTag}_${imgarch}_artifacts"
      export GIT_INFO="commit=${CI_COMMIT_SHORT_SHA} ref=${CI_COMMIT_REF_NAME} date=${CI_COMMIT_TIMESTAMP} author=${CI_COMMIT_AUTHOR} title=${CI_COMMIT_TITLE}"
  after_script:
    - docker.logout

check_artifacts:
  stage: build
  image: ${TOOLBOX_IMG}
  script:
  - |
    touch buildvars
    if [ "${CI_COMMIT_BRANCH}" = "master" ]; then
      for arch in "amd64" "arm" "arm64" "debug"; do
        img="${CI_REGISTRY_IMAGE}:${MAGICMIRROR_VERSION}_${arch}_artifacts"
        echo "checking $img ..."
        [ "$(skopeo inspect docker://${img})" ] || echo "${arch}_" >> buildvars
      done
      cat buildvars
    fi
  rules:
  - if: $TASK == "runtime"
  artifacts:
    paths:
    - buildvars

build_artifacts:
  stage: build
  needs:
  - job: check_artifacts
    artifacts: true
  script:
  - |
    set -e
    if [ "${CI_COMMIT_BRANCH}" = "master" ]; then
      cat buildvars
      if [ "$(cat buildvars) | grep ${imgarch}_)" != "${imgarch}_" ]; then
        echo "no builder image rebuild"
        exit 0
      fi
      echo "CI_COMMIT_BRANCH is master"
      BuildRef="${MAGICMIRROR_VERSION}"
    else
      echo "CI_COMMIT_BRANCH is not master"
      BuildRef="develop"
    fi
    set | grep -E "BUILDER_IMG=|NODE_VERSION=|buildarch=|BuildRef=|GitRepo=|PRE_IMGARCH="
    build --context "./build" \
    --dockerfile "Dockerfile-artifacts" \
    --destination "${BUILDER_IMG}" \
    --build-arg NODE_VERSION="${NODE_VERSION}" \
    --build-arg GIT_INFO="${GIT_INFO}" \
    --build-arg buildarch="${buildarch}" \
    --build-arg BuildRef="${BuildRef}" \
    --build-arg GitRepo="${GitRepo}" \
    --build-arg debug="${debug}"
  extends: 
  - .beforescript
  - .rule
  parallel:
    matrix:
      - imgarch: ["amd64", "arm", "arm64", "debug"]

.runtime:
  stage: build
  script:
  - |
    set -e
    [ -z "${SLIM}" ] && export FAT="_fat"
    [ -z "${debug}" ] && export NODE_ENV="production" || export NODE_ENV="test"
    set | grep -E "BUILDER_IMG=|NODE_VERSION=|buildarch=|FAT="
    build --context "./build" \
      --dockerfile "Dockerfile-debian" \
      --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}${FAT}_${imgarch}" \
      --build-arg NODE_VERSION="${NODE_VERSION}" \
      --build-arg DEBIAN_VERSION="${DEBIAN_VERSION}" \
      --build-arg GIT_INFO="${GIT_INFO}" \
      --build-arg buildarch="${buildarch}" \
      --build-arg BUILDER_IMG="${BUILDER_IMG}" \
      --build-arg SLIM="${SLIM}" \
      --build-arg NODE_ENV="${NODE_ENV}"
  extends: 
  - .beforescript
  - .rule

build_amd64:
  variables:
    imgarch: "amd64"
    SLIM: "-slim"
  needs: ["build_artifacts: [amd64]"]
  extends: .runtime

build_amd64_fat:
  variables:
    imgarch: "amd64"
  needs: ["build_artifacts: [amd64]"]
  extends: .runtime

build_arm:
  variables:
    imgarch: "arm"
    SLIM: "-slim"
  needs: ["build_artifacts: [arm]"]
  extends: .runtime

build_arm_fat:
  variables:
    imgarch: "arm"
  needs: ["build_artifacts: [arm]"]
  extends: .runtime

build_arm64:
  variables:
    imgarch: "arm64"
    SLIM: "-slim"
  needs: ["build_artifacts: [arm64]"]
  extends: .runtime

build_arm64_fat:
  variables:
    imgarch: "arm64"
  needs: ["build_artifacts: [arm64]"]
  extends: .runtime

build_debug:
  variables:
    imgarch: "debug"
    SLIM: "-slim"
  needs: ["build_artifacts: [debug]"]
  extends: .runtime

sync_slim:
  image: ${TOOLBOX_IMG}
  needs:
  - build_amd64
  - build_arm64
  - build_arm
  stage: build
  script:
  - |
    set -e
    set | grep -E "CI_COMMIT_BRANCH=|CI_REGISTRY_IMAGE=|MAGICMIRROR_VERSION="
    docker.gitlab.login
    if [ "${CI_COMMIT_BRANCH}" = "master" ]; then
      docker.manifest "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}" "latest"
      docker.manifest "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}" "${MAGICMIRROR_VERSION}"
      docker.sync "${CI_REGISTRY_IMAGE}:latest ${CI_REGISTRY_IMAGE}:${MAGICMIRROR_VERSION}"
    else
      docker.manifest "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}" "${CI_COMMIT_BRANCH}"
      docker.sync "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}"
    fi
  after_script:
    - docker.logout
  extends: 
  - .rule

sync_fat:
  image: ${TOOLBOX_IMG}
  needs:
  - build_amd64_fat
  - build_arm64_fat
  - build_arm_fat
  stage: build
  script:
  - |
    set -e
    set | grep -E "CI_COMMIT_BRANCH=|CI_REGISTRY_IMAGE=|MAGICMIRROR_VERSION="
    docker.gitlab.login
    if [ "${CI_COMMIT_BRANCH}" = "master" ]; then
      docker.manifest "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_fat" "fat"
      docker.manifest "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_fat" "${MAGICMIRROR_VERSION}_fat"
      docker.sync "${CI_REGISTRY_IMAGE}:fat ${CI_REGISTRY_IMAGE}:${MAGICMIRROR_VERSION}_fat"
    else
      docker.manifest "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_fat" "${CI_COMMIT_BRANCH}_fat"
      docker.sync "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_fat"
    fi
  after_script:
    - docker.logout
  extends: 
  - .rule

build_alpine:
  needs: ["build_artifacts: [amd64]"]
  variables:
    imgarch: "amd64"
  stage: build
  script:
  - |
    set -e
    set | grep -E "BUILDER_IMG=|NODE_VERSION="
    dest="--destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_alpine"
    if [ "${CI_COMMIT_BRANCH}" = "master" ]; then
      dest="${dest} --destination ${CI_REGISTRY_IMAGE}:alpine"
    fi
    build --context "./build" \
      --dockerfile "Dockerfile-alpine" \
      ${dest} \
      --build-arg NODE_VERSION="${NODE_VERSION}" \
      --build-arg GIT_INFO="${GIT_INFO}" \
      --build-arg BUILDER_IMG="${BUILDER_IMG}"
  extends: 
  - .beforescript
  - .rule

sync_alpine:
  needs: 
  - build_alpine
  image: ${TOOLBOX_IMG}
  stage: build
  script:
  - |
    set -e
    if [ "${CI_COMMIT_BRANCH}" = "master" ]; then
      docker.sync "${CI_REGISTRY_IMAGE}:alpine"
    else
      docker.sync "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_alpine"
    fi
  extends: 
  - .rule

build_distroless:
  variables:
    imgarch: "amd64"
  stage: build
  script:
  - |
    set -e
    docker.gitlab.login
    export NODE_VERSION="16"
    export BUILDER_IMG="${CI_REGISTRY_IMAGE}:develop_${imgarch}_artifacts"
    set | grep -E "BUILDER_IMG=|NODE_VERSION="
    build --context ./build \
      --dockerfile Dockerfile-distroless \
      --destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_distroless \
      --build-arg NODE_VERSION=${NODE_VERSION} \
      --build-arg BUILDER_IMG=${BUILDER_IMG}
  rules:
  - if: $TASK == "distroless"
  after_script:
    - docker.logout

test_alpine:
  stage: test
  needs: ["build_alpine"]
  variables:
    GIT_STRATEGY: none
  image: 
    name: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_alpine
    entrypoint: [""]
  script:
  - |
    set -e
    cd /opt/magic_mirror
    git log -1 > /tmp/image.txt
    if [ "${CI_COMMIT_BRANCH}" = "master" ]; then
      echo "CI_COMMIT_BRANCH is master"
      BuildRef="${MAGICMIRROR_VERSION}"
    else
      echo "CI_COMMIT_BRANCH is not master"
      BuildRef="develop"
    fi
    cd /tmp
    git clone --depth 1 -b "${BuildRef}" --single-branch "${GitRepo}" mm
    cd mm
    git log -1 > /tmp/clone.txt
    cat /tmp/image.txt
    echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    cat /tmp/clone.txt
    echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    diff /tmp/image.txt /tmp/clone.txt
    echo "no diffs detected."
  extends: .rule

# test raspi-image with gpio support and python
build_gpio:
  stage: build
  variables:
    imgarch: "arm"
  script:
  - |
    set -e
    docker.gitlab.login
    build --context "./build" \
      --dockerfile "Dockerfile-gpio" \
      --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_gpio" \
      --build-arg BASE_IMG="${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_arm"
  rules:
  - if: $TASK == "gpio"

test_debug:
  stage: test
  needs: ["build_debug"]
  variables:
    GIT_STRATEGY: none
    StartEnv: test
  image: 
    name: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_debug
    entrypoint: [""]
  script:
  - /opt/magic_mirror/entrypoint.sh
  extends: .rule

snyk_amd64:
  stage: test
  image: 
    name: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_debug
    entrypoint: [""]
  script:
  - sudo npm install -g snyk npm-check-updates
  - cd /opt/magic_mirror
  - ncu --deep
  - ncu --deep --target minor
  - snyk auth ${SNYK_TOKEN}
  - snyk test --all-projects
  rules:
  - if: $TASK == "snyk"

pages:
  stage: build
  needs: []
  image: ${CI_REGISTRY}/khassel/jekyll:latest
  script:
    - cp -v .gitlab-ci.yml ${CI_PROJECT_DIR}/pages/_data/gitlab.yml
    - uglify.sh ${CI_PROJECT_DIR}/pages/assets/js
    - cd pages
    - cp -v $HOME/Gemfile .
    - bundle exec jekyll build -d ${CI_PROJECT_DIR}/public
  artifacts:
    paths:
      - public
  rules:
  - if: ($CI_COMMIT_BRANCH == "master" && $TASK == "runtime") || ($TASK == "pages")

# see https://docs.gitlab.com/ee/user/application_security/container_scanning/
# see https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml
container_scanning:
  variables:
    DOCKER_IMAGE: karsten13/magicmirror:develop
    DOCKERFILE_PATH: build/Dockerfile-debian
    GIT_STRATEGY: fetch
  rules:
  - if: $TASK == "containerscan"

triage:
  # Links:
  # https://about.gitlab.com/handbook/marketing/strategic-marketing/getting-started/105/
  # https://medium.com/analytics-vidhya/gitlab-triage-bot-ba8afca4440a
  # https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage/-/blob/master/README.md
  stage: test
  image: ruby:2.7
  script:
    - gem install gitlab-triage
    - gitlab-triage --dry-run --token $TRIAGE_API_TOKEN --source projects --source-id $CI_PROJECT_PATH
  rules:
  - if: $TASK == "triage"

sync_readme:
  extends: .docker_readme
  rules:
  - if: $TASK == "readme"
  - if: $CI_COMMIT_BRANCH == "master"
    changes:
    - README.md
